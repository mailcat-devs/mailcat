use mailparse::{DispositionType, MailHeaderMap, ParsedMail};

#[derive(PartialEq, Eq, Debug, Default)]
pub struct Parts {
    pub text: Option<String>,
    pub html: Option<String>,
    pub attachments: Vec<Attachment>,
}

#[derive(PartialEq, Eq, Debug)]
pub struct Attachment {
    pub filename: Option<String>,
    pub ctype: mime::Mime,
    pub body: Vec<u8>,
}

impl Attachment {
    fn from_part(part: &ParsedMail) -> anyhow::Result<Self> {
        let content_disposition = dbg!(part.get_content_disposition());
        Ok(Self {
            filename: content_disposition.params.get("filename").cloned(),
            ctype: part.ctype.mimetype.parse()?,
            body: part.get_body_raw()?,
        })
    }
}

pub trait ParsedMailExt {
    fn is_attachment(&self) -> bool;
    fn subject(&self) -> Option<String>;
    fn date(&self) -> Result<Option<chrono::DateTime<chrono::FixedOffset>>, anyhow::Error>;
    fn parts(&self) -> Result<Parts, anyhow::Error>;
}

fn find_body(message: &ParsedMail, parts: &mut Parts) -> Result<(), anyhow::Error> {
    if !message.subparts.is_empty() {
        for subpart in &message.subparts {
            find_body(subpart, parts)?;
        }
    } else {
        if message.is_attachment() {
            parts.attachments.push(Attachment::from_part(message)?);
            return Ok(());
        }
        let mimetype: mime::Mime = message.ctype.mimetype.parse()?;
        if mimetype == mime::TEXT_PLAIN {
            parts.text.get_or_insert(message.get_body()?);
        } else if mimetype == mime::TEXT_HTML {
            parts.html.get_or_insert(message.get_body()?);
        }
    }
    Ok(())
}

impl<'a> ParsedMailExt for ParsedMail<'a> {
    fn is_attachment(&self) -> bool {
        self.get_content_disposition().disposition == DispositionType::Attachment
    }

    fn subject(&self) -> Option<String> {
        self.headers.get_first_value("Subject")
    }
    fn date(&self) -> Result<Option<chrono::DateTime<chrono::FixedOffset>>, anyhow::Error> {
        let date = self
            .headers
            .get_first_value("Date")
            .map(|s| chrono::DateTime::parse_from_rfc2822(&s))
            .transpose()?;
        Ok(date)
    }

    fn parts(&self) -> Result<Parts, anyhow::Error> {
        let mut body = Parts::default();
        find_body(self, &mut body)?;
        Ok(body)
    }
}

#[cfg(test)]
mod test {
    use chrono::TimeZone;

    use super::*;

    const TEXT_PLAIN: &str = r#"From: Andrey Golovizin <ag@sologoc.com>
To: ag@sologoc.com
Subject: Plain text body
Date: Sat, 17 Jul 2021 18:04:10 +0200
Message-ID: <2148453.iZASKD2KPV@sakuragaoka>
MIME-Version: 1.0
Content-Transfer-Encoding: quoted-printable
Content-Type: text/plain; charset="iso-8859-1"

Prost=FD text.
"#;

    const MULTIPART_ALTERNATIVE: &str = r#"From: Andrey Golovizin <ag@sologoc.com>
Subject: Plain text with HTML
Date: Sat, 17 Jul 2021 18:13:26 +0200
Message-ID: <3609140.kQq0lBPeGt@sakuragaoka>
MIME-Version: 1.0
Content-Type: multipart/alternative; boundary="nextPart3377186.iIbC2pHGDl"
Content-Transfer-Encoding: 7Bit

This is a multi-part message in MIME format.

--nextPart3377186.iIbC2pHGDl
Content-Transfer-Encoding: quoted-printable
Content-Type: text/plain; charset="iso-8859-1"

Prost=FD text.
--nextPart3377186.iIbC2pHGDl
Content-Transfer-Encoding: quoted-printable
Content-Type: text/html; charset="UTF-8"

<strong>Tu=C4=8Dn=C3=BD HTML text</strong>
--nextPart3377186.iIbC2pHGDl--

    "#;

    const MULTIPART_MIXED_ALTERNATIVE: &str = r#"From: Andrey Golovizin <ag@sologoc.com>
Subject: Plain text with HTML and attachment
Date: Sat, 17 Jul 2021 21:39:08 +0200
Message-ID: <3054314.5fSG56mABF@sakuragaoka>
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="nextPart1698715.VLH7GnMWUR"
Content-Transfer-Encoding: 7Bit

This is a multi-part message in MIME format.

--nextPart1698715.VLH7GnMWUR
Content-Type: multipart/alternative; boundary="nextPart5630828.MhkbZ0Pkbq"
Content-Transfer-Encoding: 7Bit

This is a multi-part message in MIME format.

--nextPart5630828.MhkbZ0Pkbq
Content-Transfer-Encoding: base64
Content-Type: text/plain; charset="UTF-8"

UHJvc3TDvSB0ZXh0Lg==


--nextPart5630828.MhkbZ0Pkbq
Content-Transfer-Encoding: quoted-printable
Content-Type: text/html; charset="UTF-8"

<strong>Tu=C4=8Dn=C3=BD HTML text</strong>
--nextPart5630828.MhkbZ0Pkbq--

--nextPart1698715.VLH7GnMWUR
Content-Disposition: attachment; filename="test.txt"
Content-Transfer-Encoding: base64
Content-Type: text/plain; charset="UTF-8"; name="test.txt"

TsSbamFrw6EgcMWZw61sb2hhLgo=


--nextPart1698715.VLH7GnMWUR--
"#;

    #[test]
    fn test_plain_text_body() -> Result<(), anyhow::Error> {
        let message = mailparse::parse_mail(TEXT_PLAIN.as_bytes())?;
        assert_eq!(message.subject(), Some("Plain text body".to_string()));
        assert_eq!(
            message.date()?,
            Some(
                chrono::FixedOffset::east(3600 * 2)
                    .ymd(2021, 7, 17)
                    .and_hms(18, 4, 10),
            )
        );
        assert_eq!(
            message.parts()?,
            Parts {
                text: Some("Prostý text.".to_string()),
                html: None,
                ..Default::default()
            },
        );
        Ok(())
    }

    #[test]
    fn test_multipart_alternative_text_body() -> Result<(), anyhow::Error> {
        let message = mailparse::parse_mail(MULTIPART_ALTERNATIVE.as_bytes())?;
        assert_eq!(message.subject(), Some("Plain text with HTML".to_string()));
        assert_eq!(
            message.date()?,
            Some(
                chrono::FixedOffset::east(3600 * 2)
                    .ymd(2021, 7, 17)
                    .and_hms(18, 13, 26),
            )
        );
        assert_eq!(
            message.parts()?,
            Parts {
                text: Some("Prostý text.".to_string()),
                html: Some("<strong>Tučný HTML text</strong>".to_string()),
                ..Default::default()
            },
        );
        Ok(())
    }

    #[test]
    fn test_multipart_mixed_alternative_text_body() -> Result<(), anyhow::Error> {
        let message = mailparse::parse_mail(MULTIPART_MIXED_ALTERNATIVE.as_bytes())?;
        assert_eq!(
            message.subject(),
            Some("Plain text with HTML and attachment".to_string())
        );
        assert_eq!(
            message.date()?,
            Some(
                chrono::FixedOffset::east(3600 * 2)
                    .ymd(2021, 7, 17)
                    .and_hms(21, 39, 8),
            )
        );
        assert_eq!(
            message.parts()?,
            Parts {
                text: Some("Prostý text.".to_string()),
                html: Some("<strong>Tučný HTML text</strong>".to_string()),
                attachments: vec![Attachment {
                    filename: Some("test.txt".to_string()),
                    ctype: mime::TEXT_PLAIN,
                    body: "Nějaká příloha.\n".into(),
                }]
            },
        );
        Ok(())
    }
}
