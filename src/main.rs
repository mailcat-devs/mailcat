mod parsing;

use parsing::ParsedMailExt;

use clap::Clap;

use std::io::Read;

/// MIME mail viewer.
#[derive(Clap, Debug)]
#[clap(version=clap::crate_version!())]
struct Opts {
    filename: Option<String>,
}

fn main() -> Result<(), anyhow::Error> {
    let opts = Opts::parse();
    let input = match opts.filename {
        Some(filename) => std::fs::read(filename)?,
        None => {
            let mut result = Vec::new();
            std::io::stdin().read_to_end(&mut result)?;
            result
        }
    };
    let message = mailparse::parse_mail(&input)?;
    println!("Subject: {}", message.subject().unwrap_or_default());
    if let Some(date) = message.date()? {
        println!("Date: {}", date);
    }
    println!("---");
    let parts = message.parts()?;
    println!("{}", parts.text.unwrap_or_default());
    for (i, attachment) in parts.attachments.iter().enumerate() {
        println!(
            "[{}] {}",
            i,
            &attachment.filename.as_deref().unwrap_or("<unnamed>")
        );
    }
    Ok(())
}
