{
  description = "Console mail reader";
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nmattia/naersk";
  };

  outputs = { self, nixpkgs, flake-utils, naersk }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = import nixpkgs {
          inherit system;
        };
        naersk-lib = naersk.lib."${system}";
      in
        rec {
          packages.mailcat = naersk-lib.buildPackage {
            pname = "mailcat";
            root = ./.;
            doCheck = true;
          };
          defaultPackage = packages.mailcat;
          apps.mailcat = flake-utils.lib.mkApp {
            drv = packages.mailcat;
          };
          defaultApp = apps.mailcat;
          devShell = pkgs.mkShell {
            name = "mailcat";
            buildInputs = [
              pkgs.cargo
              pkgs.clippy
              pkgs.rust-analyzer
              pkgs.rustc
              pkgs.rustfmt
            ];
          };
        }
    );
}
